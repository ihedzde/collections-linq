using System.Collections.Generic;
using collections_linq.Domain.Models;

namespace collections_linq.DTOs
{
    public class IdUserAndTasks
    {
        public int Id { get; set; }
        public UserModel User { get; set; }
        public IList<TaskModel> Tasks { get; set; }
    }
}