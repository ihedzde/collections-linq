namespace collections_linq.DTOs
{
    public struct IdAndName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}