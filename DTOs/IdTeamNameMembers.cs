using System.Collections.Generic;
using collections_linq.Domain.Models;

namespace collections_linq.DTOs
{
    public struct IdTeamNameMembers
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public IList<UserModel> Members{ get; set; }
    }
}