using System;
using collections_linq.Domain.Models;
using collections_linq.Domain.Networking;
using System.Threading.Tasks;
using System.Linq;
using collections_linq.Domain.Interfaces;
using System.Collections.Generic;
using collections_linq.DTOs;
namespace collections_linq.Services
{
    public class SelectServices
    {
        private ProjectService _projectService;
        public SelectServices()
        {
            Init();
        }
        private void Init()
        {
            IRepository<UserModel> userRepo = new UserRepository();
            IRepository<TeamModel> teamRepo = new TeamRepository(userRepo);
            IRepository<TaskModel> taskRepo = new TaskRepository();
            _projectService = new ProjectService(taskRepo, userRepo, teamRepo);
        }
        public async Task<Dictionary<string, int>> GetTasksEntitiesByIdAsync(int userId) =>
        (await _projectService.GetProjectsAsync()).Select(project => new KeyValuePair<string, int>(project.Name, project.Tasks.Count(task => task.Performer.Id == userId))).Where(x => x.Value != 0).ToDictionary(x => x.Key, x => x.Value);

        public async Task<IList<TaskModel>> GetLessThan45Async(int userId) =>
         (await _projectService.GetProjectsAsync()).SelectMany(project => project.Tasks).Where(task => task.PerformerId == userId && task.Name.Length < 45).ToList();
        public async Task<IList<IdAndName>> FinishedIn2021Async(int userId) =>
         (await _projectService.GetProjectsAsync()).SelectMany(project => project.Tasks).Where(task => task.FinishedAt?.Year == 2021 && task.Performer.Id == userId)
            .Select(task => new IdAndName { Id = task.Id, Name = task.Name }).ToList();
        public async Task<IList<IGrouping<string, IdTeamNameMembers>>> TeamsOlder10SortedAsync() =>
        (await _projectService.GetProjectsAsync())
            .Select(project => new
            {
                Id = project.Team.Id,
                Name = project.Team.Name,
                CreatedYear = project.Team.CreatedAt.Year,
                Users = project.Team.Members
            }
            )
            .OrderByDescending(entity => entity.CreatedYear)
            .Select(entity => new IdTeamNameMembers
            {
                Id = entity.Id,
                TeamName = entity.Name,
                Members = entity.Users.ToList()
            })
            .GroupBy(entity => entity.TeamName).ToList();
        public async Task<IList<IdUserAndTasks>> UsersAlphabeticByTasksLength() =>
        (await _projectService.GetUsersAsync())
            .OrderBy(x => x.FirstName).GroupJoin(
                (await _projectService.GetProjectsAsync()).SelectMany(x => x.Tasks),
                user => user.Id,
                task => task.PerformerId,
                (user, task) => new IdUserAndTasks
                {
                    Id = user.Id,
                    User = user,
                    Tasks = task.OrderByDescending(t => t.Name.Length).ToList()
                }
            ).ToList();

        async Task<dynamic> ComplexUserEntity(int userId)
       => (await _projectService.GetProjectsAsync()).Where(
                project => project.AuthorId == userId
            ).OrderByDescending(x => x.CreatedAt)
            .Select(
                p => new
                {
                    User = p.Author,
                    TaskCount = p.Tasks.Count(),
                    UnfinishedTaskCount = p.Tasks.Count(task => task.PerformerId == userId && task.State != 2 && task.State != 3),//2 - cancelled 3 - unfinished tasks
                     LongestTask = p.Tasks.OrderByDescending((t) => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).FirstOrDefault()
                }
            ).FirstOrDefault();

        async Task<dynamic> OddlyProjectTaskUserCountMix() =>
       await Task.WhenAll((await _projectService.GetProjectsAsync())
           .Select<ProjectModel, Task<dynamic>>(async project =>
           project.Description.Length > 20 && project.Tasks.Count() < 3 ?
           new
           {
               Project = project,
               LongestDescriptionTask = project.Tasks.Count > 2 ?
               project.Tasks.Aggregate((t1, t2) => t1.Description.Length > t2.Description.Length ? t1 : t2)
               : null,
               ShortestNameTask = project.Tasks.Count > 2 ?
                project.Tasks.Aggregate((t1, t2) => t1.Name.Length < t2.Name.Length ? t1 : t2)
               : null,
               TotalTeamUsersCount = (await _projectService.GetUsersAsync()).Where(x => x.TeamId == project.TeamId).Count()

           } :
           new
           {
               Project = project,
               LongestDescriptionTask = project.Tasks.Count > 2 ?
               project.Tasks.Aggregate((t1, t2) => t1.Description.Length > t2.Description.Length ? t1 : t2)
               : project.Tasks.Count > 1 ? project.Tasks.First() : null,
               ShortestNameTask = project.Tasks.Count > 2 ?
                project.Tasks.Aggregate((t1, t2) => t1.Name.Length < t2.Name.Length ? t1 : t2)
               : project.Tasks.Count > 1 ? project.Tasks.First() : null,
           }
           ).ToList());
    }
}