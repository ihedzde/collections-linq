using System.Collections.Generic;
using System.Threading.Tasks;

namespace collections_linq.Domain.Interfaces
{
    public interface IRepository<T>
    {
         Task<IList<T>> Get();
         Task<T> GetById(int? id);
    }
}