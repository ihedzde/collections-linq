using System;
using System.Collections.Generic;

namespace collections_linq.Domain.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public UserModel Author { get; set; }
        public int TeamId { get; set; }
        public TeamModel Team { get; set; }
        public IList<TaskModel> Tasks{ get; set;}
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
