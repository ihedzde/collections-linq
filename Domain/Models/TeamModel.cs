using System;
using System.Collections.Generic;

namespace collections_linq.Domain.Models
{
    public class TeamModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public IList<UserModel> Members { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
