using System;

namespace collections_linq.Domain.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public UserModel Performer { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public short State { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
