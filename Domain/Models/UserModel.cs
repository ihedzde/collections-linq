using System;
using Newtonsoft.Json;

namespace collections_linq.Domain.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
