using Newtonsoft.Json;
using collections_linq.Domain.Configs;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using collections_linq.Domain.Interfaces;
using collections_linq.Domain.Models;

namespace collections_linq.Domain.Networking
{
    public class UserRepository: IDisposable, IRepository<UserModel>{
        private readonly HttpClient client;
        private readonly string _route = Routes.Users.ToString();

        public UserRepository()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(Config.FetchUrlBase);
        }

        public void Dispose()
        {
            client.Dispose();
        }

        public async Task<UserModel> GetById(int? id){
            var response = await client.GetAsync(_route+"/"+id);
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<UserModel>(json);
        }
        public async Task<IList<UserModel>> Get(){
            var response = await client.GetAsync(_route);
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<UserModel>>(json);
        }
    }
}
