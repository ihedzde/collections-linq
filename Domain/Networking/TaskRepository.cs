using Newtonsoft.Json;
using collections_linq.Domain.Configs;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using collections_linq.Domain.Interfaces;
using collections_linq.Domain.Models;
using System.Linq;

namespace collections_linq.Domain.Networking
{
    public class TaskRepository: IDisposable, IRepository<TaskModel>
    {
        private readonly HttpClient client;
        private readonly IRepository<UserModel> _userRepo;
        private readonly string _route = Routes.Tasks.ToString();

        public TaskRepository()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(Config.FetchUrlBase);
            _userRepo = new UserRepository();
        }

        public void Dispose()
        {
            client.Dispose();
        }

        public async Task<TaskModel> GetById(int? id){
            var response = await client.GetAsync(_route+"/"+id);
            var json = await response.Content.ReadAsStringAsync();
            TaskModel model =  JsonConvert.DeserializeObject<TaskModel>(json);
            model.Performer = await _userRepo.GetById(model.PerformerId);
            return model;
        }
        public async Task<IList<TaskModel>> Get(){
            var response = await client.GetAsync(_route);
            var json = await response.Content.ReadAsStringAsync();
            IList<TaskModel> models =  JsonConvert.DeserializeObject<IList<TaskModel>>(json);
            var users = await _userRepo.Get();
            foreach(var model in models){
                model.Performer = users.FirstOrDefault(x=>x.Id==model.PerformerId);
            }
            return models;
        }
    }
}
