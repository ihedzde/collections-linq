using Newtonsoft.Json;
using collections_linq.Domain.Configs;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using collections_linq.Domain.Interfaces;
using collections_linq.Domain.Models;
using System.Linq;
namespace collections_linq.Domain.Networking
{
    public class ProjectService : IDisposable
    {
        private readonly HttpClient _client;
        private readonly IRepository<TaskModel> _taskRepo;
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<TeamModel> _teamRepo;
        private readonly string _route = Routes.Projects.ToString();

        public ProjectService(IRepository<TaskModel> taskRepo, IRepository<UserModel> userRepo, IRepository<TeamModel> teamRepo)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Config.FetchUrlBase);
            _taskRepo = taskRepo;
            _userRepo = userRepo;
            _teamRepo = teamRepo;
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<ProjectModel> GetProjectByIdAsync(int? id){
            var response = await _client.GetAsync(_route+"/"+id);
            var json = await response.Content.ReadAsStringAsync();
            ProjectModel model =  JsonConvert.DeserializeObject<ProjectModel>(json);
            model.Author = await _userRepo.GetById(model.AuthorId);
            model.Team = await _teamRepo.GetById(model.TeamId);
            model.Tasks = (await _taskRepo.Get()).Where(x=>x.ProjectId == model.Id).ToList();
            return model;
        }
        public async Task<IList<ProjectModel>> GetProjectsAsync(){
            var response = await _client.GetAsync(_route);
            var json = await response.Content.ReadAsStringAsync();
            IList<ProjectModel> models =  JsonConvert.DeserializeObject<IList<ProjectModel>>(json);
            var tasks = await _taskRepo.Get();
            var teams = await _teamRepo.Get();
            var users = await _userRepo.Get();
            foreach(var model in models){
                model.Tasks = tasks.Where(x=>x.ProjectId == model.Id).ToList();
                model.Team = teams.FirstOrDefault(x=>x.Id == model.TeamId);
                model.Author = users.FirstOrDefault(x=>x.Id == model.AuthorId);
            }
            return models;
        }
        public async Task<IList<UserModel>> GetUsersAsync(){
           return await _userRepo.Get();
        }
    }
}
