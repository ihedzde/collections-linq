using Newtonsoft.Json;
using collections_linq.Domain.Configs;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using collections_linq.Domain.Interfaces;
using collections_linq.Domain.Models;

namespace collections_linq.Domain.Networking
{
    public class TeamRepository: IDisposable, IRepository<TeamModel>
    {
        private readonly HttpClient client;
        private readonly IRepository<UserModel> _userRepo;
        private readonly string _route = Routes.Teams.ToString();

        public TeamRepository(IRepository<UserModel> userRepo)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(Config.FetchUrlBase);
            _userRepo = userRepo;
        }

        public void Dispose()
        {
            client.Dispose();
        }

        public async Task<TeamModel> GetById(int? id){
            var response = await client.GetAsync(_route+"/"+id);
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TeamModel>(json);
        }
        public async Task<IList<TeamModel>> Get(){
            var response = await client.GetAsync(_route);
            var json = await response.Content.ReadAsStringAsync();
            var objects =  JsonConvert.DeserializeObject<IList<TeamModel>>(json);
            foreach(var obj in objects){
                obj.Members = (await GetUsersAsync()).Where(x=>x.TeamId == obj.Id).ToList();
            }
            return objects;
        }
        public async Task<IList<UserModel>> GetUsersAsync(){
           return await _userRepo.Get();
        }
    }
}
